import pandas as pd
import hashlib as hsh
import random as ran
import string
import numpy as np
import os

#####################################
def hashstring(mystring,saltdct):#saltpre,saltap):

  salted=saltdct["pre"]+mystring+saltdct["ap"]
  #salted=saltpre+mystring+saltap
  hashstr= hsh.sha256(salted.encode('utf-8')).hexdigest()

  return hashstr

def simplehash(mystring):
  hashstr=hsh.sha256(mystring.encode('utf-8')).hexdigest()
  return hashstr

def halfstr(mystr,side):
  n=len(mystr)//2
  if side=="l":
    halved=mystr[0:n]
  elif side=="r":
    halved=mystr[n:]
  
  return halved
  
####################################

PYTHONHASHSEED=728
os.environ["PYTHONHASHSEED"]=str(PYTHONHASHSEED)

hashdir="hashing/"


JPtab=pd.read_csv(hashdir+"BookSellsData.csv")




cols=JPtab.columns
print(cols)
nrows,ncols=JPtab.shape
print(nrows,ncols)

JPtab.drop(columns=JPtab.columns[0],axis=1,inplace=True) ## Drop the index we do not need
nrows,ncols=JPtab.shape
print(nrows,ncols)
cols=JPtab.columns
print(cols)

saltdct={}
saltdct["pre"]=hsh.sha256("A nice string to preppend".encode('utf-8')).hexdigest()
saltdct["ap"]=hsh.sha256("A less nice string to append".encode('utf-8')).hexdigest()


HashTabs=JPtab["customer"].copy()
#HashTabs=pd.DataFrame(JPtab["customer"].copy(),index=["customer"])
#HashTabs=pd.DataFrame( [JPtab["customer"].copy()],columns=["customer"],dtype=StringDtype)
#HashTabs["customer"]=HashTabs["customer"].astype(StringDtype)
HashDF=pd.DataFrame()
HashDF["hash1"]=HashTabs.apply(hashstring,saltdct=saltdct)
#print("HashTabs.columns", HashTabs.columns)

#HashDF["hash1"]=HashTabs.apply(hashstring,saltdct=saltdct)
JP2=JPtab.copy()

print(HashDF["hash1"].head(7))
print(len(HashDF["hash1"][0]))

HashDF["leftsalt"]=HashDF["hash1"].apply( halfstr,side="l")
HashDF["rightsalt"]=HashDF["hash1"].apply( halfstr,side="r")

HashDF["salted"]=HashDF["leftsalt"]+JPtab["customer"]+HashDF["rightsalt"]

HashDF["fullyhashed"]=HashDF["salted"].apply(simplehash)

#print("HashTabs cols", HashTabs.columns)
print("JP2 cols", JP2.columns)

#JP2["fullyhashed"]=HashDF["fullyhashed"]
#
JP2["customer"]=HashDF["fullyhashed"]

JP2.to_csv("BookSellsDataAnon.csv",index=False)



LUT=pd.concat([JPtab["customer"],HashDF["fullyhashed"]],axis=1)

LUT.drop_duplicates().to_csv("CustomerHashLookupTable.csv",index=False)

