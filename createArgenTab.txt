-- Tabla pesos argentos

DROP TABLE IF EXISTS ArgenPeso;

/* Creamos la tabla de la data de JP, */
CREATE TABLE ArgenPeso(
 date nchar(255),
open decimal(18,4),
high decimal(18,4),
low decimal(18,4), 
close decimal(18,4),
adj_close decimal(18,4), 
volume decimal(18,4)
);



/* cargamos la data del file csv, delimitador coma, comilla doble para strings (only?) */
LOAD DATA LOCAL INFILE "ARS=X.csv"
INTO TABLE ArgenPeso
FIELDS TERMINATED BY ","
ENCLOSED BY '"'
LINES TERMINATED BY "\n"
IGNORE 1 ROWS;


--UPDATE ArgenPeso
--SET sell_date=str_to_date(date, "%Y-%m-%d");

/* Export, por ahora hay que ver a dónde lo está tirando */
SELECT * FROM ArgenPeso INTO OUTFILE "/tmp/ArgenPeso.csv"
    FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n';

/* equivalente a head */
select * from ArgenPeso LIMIT 12;



/* Agregamos el index, big int o int, que se auto incremente, y lo 
ponemos como primera columna (FIRST) en la tabla. */
ALTER TABLE ArgenPeso
ADD COLUMN id bigint AUTO_INCREMENT PRIMARY KEY FIRST;

/* este quilombete es equivalente a un tail */
SELECT * FROM (
   SELECT * FROM ArgenPeso ORDER BY id DESC LIMIT 12
) Var1
   ORDER BY id ASC;

-- Hay que interp algunos valores linealmente, tiene varios huecos.
