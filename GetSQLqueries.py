import os
import subprocess as sbp
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import seaborn as sns
import matplotlib.cm as cm



def get_sql_commands(fsq):
  
  fsqdir=""
  fsqname=fsq.split(".")[0]
  if "/" in fsq:
    fsqdir+="/".join(fsq.split("/")[:-1]) ##
            
    fsqname=fsq.split("/")[-1].split(".")[0]
  fsq=open(fsq ,"r")
  
  cL="/*"
  cR="*/"
  
  CleanLines=[]
  opencomment=False
  for l in fsq:
    if l.isspace():
      continue
    if l.strip()[:2]=="--":
      continue
    
  
    if opencomment==False and cL in l : ## left comment exists, check whether opencomment
      opencomment=True                ### was not triggered before.
      
      
      split0=l.split(cL) ## split wrt cL
      if cR in split0[1]: ## cL and cR in both lines
        opencomment=False ## close comment
        split1=split0[1].split(cR)
        auxline=split0[0]+split1[1] ## join whatever is not a comment.
        if auxline.isspace()==False: ## either left of cL or right of cR there is code.
          CleanLines.append(auxline) ## store it
          print(auxline)
          continue ### and skip to the next line. 
        
          
      if split0[0].isspace()==False: ## no cR, check whether there is anything left of cL
        CleanLines.append(split0[0]) ### somethig left of cL, store it,
      
         
        print(l)
        continue ## skip to the next line. 

    
    if opencomment==True :
      if cR in l: ## only cR, likely comment was opened
        ## before by a cL, also verify if opencomment was triggered True by a cL.
        opencomment=False ## close down opencomment. 
        split0=l.split(cR) ## split line at cR
        if split0[1].isspace()==False: ## anything right of cR?
          CleanLines.append(split0[1]) ## store it
          print(l) 
      continue ## skip to the next line
         
    if opencomment==False : ## not inside a comment bracket?
      CleanLines.append(l.strip("\n") ) ## store the line, stripping line jumps.
  #####
  
  
  
  
  print(CleanLines)
  fwriteCL=open("CleanLines"+fsqname+".txt" ,"w")
  for clean in CleanLines:
    fwriteCL.write(clean+"\n")
  fwriteCL.close()
  CleanLines=" ".join(CleanLines)

#  print(CleanLines)
  CleanLines=CleanLines.split(";")[:-1] ### drop the last empty space
  CleanLines=[cli+";" for cli in CleanLines]
  print("******************** final split")
  print(CleanLines)
  
  fwriteCL=open("OneLiners"+fsqname+".txt" ,"w")
  for clean in CleanLines:
    fwriteCL.write(clean+"\n")
    
  fwriteCL.close()
  #  print(clean)
  #OneLiners=[]
  #OneLiners=" ".join(OneLiners)
  ### Loop en las CleanLines. sep by ;?
  
    
  ## Not perfectly safe. If there happen to be */ (closing) and /* (opening) in that order in one line, the 
  ## method can fail. 
  ## Also if comments are closed AND opened in the same line I don't expect this to work. 
  
  ## Possibly better approach: count the number of "/*" and "*/" in the sql-commends file. 
  ## 
  
  
if __name__=="__main__":
  
  sqlfiles=["createJPtab.sql","createArgenTab2.sql","Queries.sql"]
  sqldir="SQLscripts/"
  for fl in sqlfiles:
    get_sql_commands(sqldir+fl)
