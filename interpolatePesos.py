

import numpy as np
import pandas as pd


pesosDF=pd.read_csv("ARS=X.csv")
#drop multiple columns by name
#df.drop(['column_name1', 'column_name2'], axis=1, inplace=True)
#drop one column by index
#df.drop(df.columns[[0]], axis=1, inplace=True)
#drop multiple columns by index
#df.drop(df.columns[[0,2,5]], axis=1, inplace=True)

#drop one column by name
pesosDF.drop('Volume', axis=1, inplace=True)


print(pesosDF.head())
print(pesosDF.describe())


#pesosDF["days"]

print(pesosDF["Date"].dtype )

pesosDF["Date"]=pd.to_datetime(pesosDF["Date"],format="%Y-%m-%d")
print(pesosDF["Date"].dtype )


pesosDF["days"]=(pesosDF['Date'] - pesosDF['Date'][0]) / np.timedelta64(1, 'D')

print(pesosDF["days"].head())
print(pesosDF["days"].tail())


xvec0=pesosDF.dropna()["days"]
yvec0=pesosDF.dropna()["Adj Close"]
xvec1=pesosDF["days"]
pesosDF["Adj Close 2"]=np.interp(xvec1,xvec0,yvec0)


pesosDF["Adj Close"].interpolate(method="linear",axis=0)

pesosDF[["Date","days","Adj Close 2"]]b.to_csv("ArgenPesosInterp.csv")



