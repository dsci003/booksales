--Orders.OrderID, Customers.CustomerName, Orders.OrderDate

drop table if exists JPsellUSD;
create table if not exists JPsellUSD as
SELECT BookSells.id, ArgenPeso.adj_close as sell_usd 
FROM BookSells
LEFT JOIN ArgenPeso 
ON ArgenPeso.date=BookSells.purchase_date;

select * from JPsellUSD LIMIT 5;


drop table if exists JPbuyUSD;
create table if not exists JPbuyUSD as
SELECT BookSells.id, ArgenPeso.adj_close as buy_usd 
FROM BookSells
LEFT JOIN ArgenPeso 
ON ArgenPeso.date=BookSells.purchase_date;

select * from JPbuyUSD LIMIT 5;


--create table if not exists JPUSD as
--SELECT BookSells.id, ArgenPeso.adj_close as buy_usd 
--FROM BookSells
--LEFT JOIN ArgenPeso 
--ON ArgenPeso.date=BookSells.buy_date;

/* cumbersome export, not needed now that connectors are in use */
--SELECT * FROM JPsellUSD INTO OUTFILE "/tmp/JPsellUSD.csv"
--    FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n';

/* cumbersome export, not needed now that connectors are in use */
-- SELECT * FROM JPbuyUSD INTO OUTFILE "/tmp/JPbuyUSD.csv"
--    FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n';


/* a join  */
/*  They come from a left join, they have the original tab rows */

drop table if exists BookSellsUSD;
create table if not exists BookSellsUSD as
select *
from BookSells;

select * from BookSellsUSD LIMIT 12;



/* estos son los precios del dólar, pero no lo puedo cargar */
ALTER TABLE BookSellsUSD 
ADD COLUMN usd_buyprice decimal(18,4) ,
ADD COLUMN usd_sellprice decimal(18,4)  ;
select * from BookSellsUSD LIMIT 10;


UPDATE BookSellsUSD SET usd_buyprice = JPbuyUSD.buy_usd;
UPDATE BookSellsUSD SET usd_sellprice = JPbuyUSD.sell_usd;

select * from BookSellsUSD LIMIT 10;
