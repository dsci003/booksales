-- Table: Argentinian pesos

DROP TABLE IF EXISTS ArgenPeso;

/* We create the AR$ table */
CREATE TABLE ArgenPeso(
id bigint,
date nchar(255),
days decimal(18,4),
adj_close decimal(18,4)
);



/* We load the interpolated AR$ table from the csv file. */
LOAD DATA LOCAL INFILE "ArgenPesosInterp.csv"
INTO TABLE ArgenPeso
FIELDS TERMINATED BY ","
ENCLOSED BY '"'
LINES TERMINATED BY "\n"
IGNORE 1 ROWS;


--UPDATE ArgenPeso
--SET sell_date=str_to_date(date, "%Y-%m-%d");

/* Export to an above-home location, rather impractical. */
/*SELECT * FROM ArgenPeso INTO OUTFILE "/tmp/ArgenPeso2.csv"
    FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n'; */

/* equivalent to a head command */
select * from ArgenPeso LIMIT 12;



/* We add a big int or int index that self increments,
and we set it as the first column (FIRST) on the tabla. */
--ALTER TABLE ArgenPeso
--ADD COLUMN id bigint AUTO_INCREMENT PRIMARY KEY FIRST;

/* the following equates to a tail command */
SELECT * FROM (
   SELECT * FROM ArgenPeso ORDER BY id DESC LIMIT 12
) Var1
   ORDER BY id ASC;


