--If it already exists, we drop it
DROP TABLE IF EXISTS BookSells;

/* We create the Book Sells table data structure*/
CREATE TABLE BookSells(
 purchase_date nchar(255),
 sell_date nchar(255),
days_held bigint,
author nchar(255),
title nchar(255),
price_bought decimal(18,4),
price_sold decimal(18,4),
profit decimal(18,4), 
customer nchar(255)
);




/* We load the data from the csv file, delimiting with colon, double quotes for strings */
LOAD DATA LOCAL INFILE "BookSellsDataAnon.csv"
INTO TABLE BookSells
FIELDS TERMINATED BY ","
ENCLOSED BY '"'
LINES TERMINATED BY "\n"
IGNORE 1 ROWS;


/* converting the dates from nchar to date format */
UPDATE BookSells
SET purchase_date=str_to_date(purchase_date, "%d/%m/%Y");
/* had to repeat the update, it cannot operate on both sets simultaneously*/
UPDATE BookSells
SET sell_date=str_to_date(sell_date, "%d/%m/%Y");


/* We add a self-increment index, and set it 
as the first colum (FIRST) on the table. */
ALTER TABLE BookSells
ADD COLUMN id bigint AUTO_INCREMENT PRIMARY KEY FIRST;


/* Export, mariadb exports to a director above /home, which is inconvenient */
/*SELECT * FROM BookSells INTO OUTFILE "/tmp/JPexported.csv"
    FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n';*/
/* Export commented out, as the output will be funneled to a python script*/

/* equivalent to a head */
/*select * from BookSells LIMIT 12;*/
/* The head command analog will be done in the python scrpt*/


/* cumbersome, but equivalent to a tail */
/*SELECT * FROM (
   SELECT * FROM BookSells ORDER BY id DESC LIMIT 12
) Var1
   ORDER BY id ASC;*/
/*This tail equivalent will not be needed, as the tables will be gathered by the python script*/

