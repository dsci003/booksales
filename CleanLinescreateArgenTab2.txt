DROP TABLE IF EXISTS ArgenPeso;

CREATE TABLE ArgenPeso(
id bigint,
date nchar(255),
days decimal(18,4),
adj_close decimal(18,4)
);

LOAD DATA LOCAL INFILE "ArgenPesosInterp.csv"
INTO TABLE ArgenPeso
FIELDS TERMINATED BY ","
ENCLOSED BY '"'
LINES TERMINATED BY "\n"
IGNORE 1 ROWS;



select * from ArgenPeso LIMIT 12;


SELECT * FROM (
   SELECT * FROM ArgenPeso ORDER BY id DESC LIMIT 12
) Var1
   ORDER BY id ASC;
