# Book seller records

The analysis is performed in the *BookSales.ipynb* notebook. Its Python version is in the *.py* file with the same name. Having said that, there are a number of supplementary scripts and data files that are described below, for instance, the anonymization script or the one used to streamline SQL queries so that they introduce no errors when passed as Python strings. 

## Creating the python environment for the project 


We went the route of installing every needed python package inside a virtual environment.  
 `sudo apt install python3-venv python3-pip`

`pip install simplejson numpy scipy pandas matplotlib seaborn jupyter mkl scikit-learn`  

Had a dependency issue with zlib. Pillow, had to specify 
`pip install pillow==6.2.2`
zlib WAS installed, but the default Pillow version was not detecting it.
 



The python connector requires at least the maridb package version 10.9.
Debian 10 has version 10.3, which contains the connector/C 3.1.21, so I added the mariadb repository manually to apt sources and subsequently installed, using apt but not from the Debian 10 repo but the more up-to-date mariabd repo, which has the required versions. 
Further indications in the following link:  
https://mariadb.com/docs/skysql-previous-release/connect/programming-languages/c/install/#Configure_Package_Repository_(Linux)

`sudo apt install libmariadb3 libmariadb-dev`

By default it has a root user with the system-root-user password. From there you have to create the user we plan to use through the connector, and issue its password. 

`pip install mariadb`


## Initial datasets


*hashing/BookSellsData.csv* : contains all the transaction history of a book seller in Argentina who has been reselling preowned books. 

*ARS=X.csv* : As the original data comes from Argentina, a country which has a history of currency devaluation, an additional input was required. This table contains the vale of a single US$ in AR$ over the years. Although the dollar is not inflation-free, its depreciation takes place over years to decades, whereas currently the AR$ loses half its value over one year. This table will enable conversion from the local currency to a more stable one in order to compare the income, profit margins and any value-related quantity to a somewhat steady reference. The table, however, contains the official AR$-US$ conversion rates, which are not the rates accessible to everyday people, therefore the correction may not be able to fully revert the effect of the Peso depreciation. 

*DollarBlue.csv* : non-Argentinians may be surprised at this, but there are actually more than ten different USD valuations. There is an official value, but people cannot regularly acquire USD at that convenient rate. There is is a free market dollar which can be bought and sold behind the back of the government, and it is named Dollar Blue. Since this is the value at which people can actually obtain USD, they are included in the study. In contrast to the official value, many websites offer plots, but not the chart data in downloadable form. The quickest solution, straight from my physics days when I needed to extract data from a plot, was to digitize the curve using Engauge Digitizer.

*DollarBlue2.csv" : same file, with renamed columns. This is the file read by the main notebook. Made by the DollarBlue (short) notebook.

*DBlueInterp.csv" : It is the Dollar Blue data interpolated to the transaction dates, it is not read but produced by the main notebook.

## Anonymization

The customer names have been hashed so they are not part of the set. They were prehashed by using similar methods to the code HashJPnames.py, however with different parameters, and that prehasher is not part of the repo, so the "names" are only hashes from the get go. 

## HashJPnames.py

We will use the following hashing strategy to make it extremely laborius to brute force the customer names. The strategy means that hypothetical attacker would have to bruteforce each customer name individually, which is an already formidable task, and pushes the bruteforcing of the entire set further down into the realm of implausibility.

Two salt phrases are predefined for --and shared by-- the whole set. One is preppended, the other is appended to the customer full name. Then, the resulting string is hashed, leading to user-dependent hashes. These hashes are split in half and preppended-appended to the customer names, and hashed once more. This way, a brute-force attacker would have to carry out an entire attempt for each and every username. 

As this dataset is tiny compared to $2^{256}$, we do not expect clashes, and since there is no supplementary information on the customers, no disambiguation hash will be created.
For instance, if we had home addresses and phone numbers or zip codes we could create a simpler secondary hash with any combination of the aforementioned, to discern in the case of a clash in the primary hash. In hexadecimal the output string of the SHA-256 is 64 characters long.

This script exports *BookSellsDataAnon.csv* as the new starting-point dataframe for the study, as well as a lookup table in the form of *CustomerHashLookupTable.csv* which in a real case should be kept isolated from the whole analysis for security and privacy-laws reasons. 


## interpolatePesos.py 

The AR$ table includes plenty of missing values. Some could be ascribed to the heavy economic collapse the country experienced in 2001, for instance. The *interpolatePesos.py* script takes care of linearly interpolating the data to the missing values. This has to do with the official Peso value, which is only used in the first part of the notebook. 



## Installing and initializing MariaDB

Upon first booting with the root account (or requiring sudo privileges),
we can create a user and operate from that, so elevated privileges are not needed. Instructions can be found at the link below.  
https://mariadb.com/kb/en/create-user/

As the admin, one has to grant access to the project database, where all the needed tables will be stored. From there on, one can proceed with the designated user, which will be accessed via the python connector.

mysql -umdb_user -pmypassword projectDB

Start with the user, so root credentials are never handled in the process. 


## SQL queries 

The study was started using SQL. 
In order to execute SQL queries comfortably in Python via the mariadb connector, I found best to convert each query into a one liner, having eliminated the comments beforehand. This is what *GetSQLqueries.py* does.

Once the needed tables are loaded to the SQL database, they are imported to the jupyter-notebook via the connector, and the studies are continued using python and its libraries. 

The SQL credentials should be placed in the *creds/* folder as a *.json* file with the following structure:  

{
  "user": "bookseller",
  "password": "ThisIsABadPassword",
  "host": "localhost",
  "database": "booksellsdb"
} 

## Data preparation 

The *BookSellsDataAnon.csv* table has many missing values, which we will proceed to impute before using the AR$ information available in the *ArgenPeso* table. Specifically, many dates are missing, be them the purchase date or the sell date, therefore we cannot directly estimate the transaction values in USD unless we impute said dates. 


Date imputations:  
We need the purchase and sell dates to convert the AR$ values to USD.
As some dates were missing, we needed to perform an imputation. 
From the available values, the *days_held* distribution resulted approximately poisson-like. Therefore, we applied logarithms to make it a roughly normal distribution:

$ y=\log(1+x) $  
From that roughly normal distribution we obtain, random values for the transformed variable *y*.
$ y_{ran}=\normal(\hat{y},\sigma_y) $
with which we return to the original distribution:
$ x_{ran}=e^{y_{ran}}$


I ended up taking half the average and a quarter of the spread, as the method was generating transactions prior to the seller's business starting date. An improvement over this method would be to obtain the purchase and sell distributions for each year, and create a year-by-year distribution with which to impute dates. This would solve the possibly scenario where a very long holding period is imputed at a date where the seller had just started their business. 


I created columns with the USD and Dollar Blue (BLU) prices for each book purchases/sold, which required some interpolations. As much as the Peso value has degraded over the decade, the interpolated values are fair approximations to its value at the time of the transactions. 


